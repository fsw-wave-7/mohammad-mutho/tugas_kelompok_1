class AdminController {
	getAdmin = (req, res) => {
		res.send("get");
	};
	insertAdmin = (req, res) => {
		res.send("insert");
	};
	updateAdmin = (req, res) => {
		res.send("update");
	};
	deleteAdmin = (req, res) => {
		res.send("delete");
	};
}

module.exports = AdminController;
