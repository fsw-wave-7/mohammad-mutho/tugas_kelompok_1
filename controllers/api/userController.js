class UserController {
	getUser = (req, res) => {
		res.send("get");
	};
	insertUser = (req, res) => {
		res.send("insert");
	};
	updateUser = (req, res) => {
		res.send("update");
	};
	deleteUser = (req, res) => {
		res.send("delete");
	};
}

module.exports = UserController;
