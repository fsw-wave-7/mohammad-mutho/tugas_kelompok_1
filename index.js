const express = require("express");
const app = express();
const port = 3000;
const bodyParser = require("body-parser");
const dashboard = require("./routers/dashboard");
const adminApi = require("./routers/api/adminApi");
const userApi = require("./routers/api/userApi");
const path = require("path");

// Set view engine
app.set("view engine", "ejs");

// Load static file
app.use(express.static(path.join(__dirname, "public")));

// DASHBOARD
app.use("/", dashboard);

// API;
app.use("/api/admin", adminApi);
app.use("/api/user", userApi);

app.listen(port, console.log(`server sukses berjalan di port: ${port}`));
